#!/bin/sh
exec /usr/lib/rpm/find-provides | \
	sed -e s/libX11.so.6.2.debug// | sed -e s/libXrender.so.1.2.debug// | sed -e s/libXext.so.6.4.debug// | \
	sed -e s/libX11.so.6// | sed -e s/libXrender.so.1// | sed -e s/libXext.so.6//
